# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).


## [Unreleased]

## [0.0.1] — 2019-09-05
### Added
- _CHANGELOG.md_ created.
### Changed
- Something has been changed.
### Fixed
- Something has been fixed.
### Removed
- Something has been removed.


[0.0.1]: https://gitlab.com/abelxluck/ip-geoloc-contains/compare/0.0.1...0.0.1
[Unreleased]: https://gitlab.com/abelxluck/ip-geoloc-contains/compare/0.0.1...HEAD
