# ip-geoloc-contains
[![AGPL License](http://img.shields.io/badge/license-AGPL%20v3-red.svg?style=flat)](http://opensource.org/licenses/AGPL-3.0)
[![Build Status](https://gitlab.com/abelxluck/ip-geoloc-contains/badges/master/pipeline.svg)](https://gitlab.com/abelxluck/ip-geoloc-contains/pipelines)
[![codecov](https://codecov.io/gl/abelxluck/ip-geoloc-contains/branch/master/graph/badge.svg)](https://codecov.io/gl/abelxluck/ip-geoloc-contains)
[![Clojars Project](https://img.shields.io/clojars/v/ip-geoloc-contains.svg)](https://clojars.org/ip-geoloc-contains)

A Clojure microlibrary for determining an IP address(es) geolocation is inside a GeoJSON geometry.

```clj
[ip-geoloc-contains "0.0.1-SNAPSHOT"]
```

## Usage

This library is designed to be used alongside
[BrunoBonacci/ip-geoloc](https://github.com/BrunoBonacci/ip-geoloc), though it
does not explicitly depend on it. So, add this library along with
`[com.brunobonacci/ip-geoloc "0.3.0"]` to your `project.clj`.

Familiarize yourself with how to load an mmdb database file by reading
`ip-geoloc`'s README.

``` clj
;; require both namespaces
(require '[ip-geoloc.core :as [geoip]])
(require '[ip-geoloc-contains.core :as [geoipc]])

;; start the geo-ip provider and connect to the database
(def provider (geoip/start-provider {:database-file mmdb-path}))

;; define a lookup function we will pass to ip-geoloc-contains
(defn ip->geo
  "Given an `ip`, performs a lookup using the `provider` and returns the lookup result."
  [ip]
   (geoip/geo-lookup provider ip))

;; define your buckets
;;  a bucket is a label associated with a geographical region defined by geojson
;;  the region can be any combination of Polygon and MultiPolygon geometries
(def bucket-config
    [{:bucket :usa :file "example-data/usa.geojson"}
     {:bucket :benelux :file "example-data/benelux.geojson"}])

;; pre-parse the bucket regions once
(def buckets (geoipc/prepare-buckets bucket-config))

;; match an ip address to a geo bucket, this one is in Luxembourg
(geoipc/match-geo (ip->geo "5.8.40.1") buckets :other) ;; => :benelux

;; match an ip address to a geo bucket, this one is in Ohio, USA
(geoipc/match-geo (ip->geo "24.123.64.1") buckets :other) ;; => :usa

;; match an ip address to a geo bucket, this one is in Germany
(geoipc/match-geo (ip->geo "2.167.109.1") buckets :other) ;; => :other
```


## Development

### Testing

```console
$ export MMDB_PATH=/your/path/to/geoip/GeoIP2-City.mmdb
$ lein test
```
## License

Unless otherwise noted, all files © 2019-2020 Abel Luck

Distributed under the terms of the GNU Affero General Public License (AGPL)
v3.0 or any later version, with the EPL v1.0 exception.

See
[LICENSE](https://gitlab.com/abelxluck/ip-geoloc-contains/raw/master/LICENSE)
for details and EPL v1.0 exception.
