(ns ip-geoloc-contains.core
  (:require
   [geo [io :as gio] [jts :as jts]]))

(defn load-geojson
  "Loads the GeoJSON file at `filename` into a JTS object." [filename]
  (-> filename
      slurp
      gio/read-geojson))

(defn- geo->point
  "Creates a JTS point from a map that contains longitude and latitude"
  [{:keys [latitude longitude] :as geo}]
  (when (and  (some? latitude) (some? longitude))
    (jts/point latitude longitude)))

(defn feature-match-poly
  "Evaulates and returns `pred` against `geometry` iff the geometry type is Polygon or MultiPolygon, returns false otherwise."
  [pred geometry]
  (condp = (.getGeometryType geometry)
    "Polygon" (pred geometry)
    "MultiPolygon" (pred geometry)
    false))

(defn point-within?
  "Returns true if the point lies within the feature."
  [point feature]
  (.within point feature))

(defn pt-within-features?
  "Checks if `point` is geographically within any of the `features`."
  [features point]
  (some (fn [g] (feature-match-poly
                 #(point-within? point %)
                 (:geometry g)))
        features))

(defn geo-within-features?
  "Checks  if the `geo` result is geographically within any of the `features`"
  [features geo]
  (when-some [pt (geo->point geo)]
    (pt-within-features? features pt)))

(defn match-geo-to-bucket
  "Returns `bucket` when the `geo` lies within at least one of the features."
  [geo {:keys [bucket features]}]
  (when (geo-within-features? features geo)
    bucket))

(defn match-geo
  ""
  [geo buckets default-bucket]
  (or (first (remove nil?
                     (map (partial match-geo-to-bucket geo)
                          buckets)))
      default-bucket))

(defn prepare-bucket [bucket-def]
  (assoc bucket-def :features (load-geojson (:file bucket-def))))

(defn prepare-buckets [bucket-defs]
  (mapv prepare-bucket bucket-defs))
