(defproject info.guardianproject/ip-geoloc-contains "0.1.0-SNAPSHOT"
  :description "A microlibrary for determining an IP address(es) geolocation is inside a GeoJSON geometry."
  :url "https://gitlab.com/abelxluck/ip-geoloc-contains"
  :license {:name "GNU Affero General Public License 3.0 with EPL 1.0 Exceptions"
            :url  "https://gitlab.com/abelxluck/ip-geoloc-contains/raw/master/LICENSE"}
  :dependencies [[factual/geo "2.1.1"]]
  :plugins [[lein-cloverage "1.0.13"]
            [lein-shell "0.5.0"]
            [lein-ancient "0.6.15"]
            [lein-changelog "0.3.2"]]
  :profiles {:dev {:source-paths ["dev"]
                   :resource-paths ["example-data"]
                   :dependencies [[org.clojure/clojure "1.10.1"]
                                  [com.brunobonacci/ip-geoloc "0.2.3" :exclusions [com.fasterxml.jackson.core/jackson-databind]]]}}
  :deploy-repositories [["releases" :clojars]]
  :aliases {"update-readme-version" ["shell" "sed" "-i" "s/\\\\[ip-geoloc-contains \"[0-9.]*\"\\\\]/[ip-geoloc-contains \"${:version}\"]/" "README.md"]}
  :release-tasks [["vcs" "assert-committed"]
                  ["change" "version" "leiningen.release/bump-version" "release"]
                  ["changelog" "release"]
                  ["update-readme-version"]
                  ["vcs" "commit"]
                  ["vcs" "tag"]
                  ;["deploy"]
                  ["change" "version" "leiningen.release/bump-version"]
                  ["vcs" "commit"]
                  ; ["vcs" "push"]
                  ]

  :old-release-tasks [["shell" "git" "diff" "--exit-code"]
                      ["change" "version" "leiningen.release/bump-version"]
                      ["change" "version" "leiningen.release/bump-version" "release"]
                      ["changelog" "release"]
                      ["update-readme-version"]
                      ["vcs" "commit"]
                      ["vcs" "tag"]
                      ["deploy"]
                      ["vcs" "push"]])
