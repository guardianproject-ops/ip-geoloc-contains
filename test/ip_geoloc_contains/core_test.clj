(ns ip-geoloc-contains.core-test
  (:require [clojure.test :refer :all]
            [ip-geoloc.core :as geoip]
            [ip-geoloc-contains.core :as geoipc]))

(def mmdb-path (or  (System/getenv "MMDB_PATH") "/var/lib/GeoIP/GeoLite2-City.mmdb"))

;; start the geo-ip provider and connect to the database
(def provider (geoip/start-provider {:database-file mmdb-path}))

;; define a lookup function we will pass to ip-geoloc-contains
(defn ip->geo
  "Given an `ip`, performs a lookup using the `provider` and returns the lookup result."
  [ip]
  (geoip/geo-lookup provider ip))

;; define your buckets
;;  a bucket is a label associated with a geographical region defined by geojson
;;  the region can be any combination of Polygon and MultiPolygon geometries
(def bucket-config
  [{:bucket :usa :file "example-data/usa.geojson"}
   {:bucket :benelux :file "example-data/benelux.geojson"}])

;; pre-parse the bucket regions once
(def buckets (geoipc/prepare-buckets bucket-config))

(deftest example-data
  (testing "match-geo"
    (is (= :benelux
           (geoipc/match-geo (ip->geo "5.8.40.1") buckets :other)))
    (is (= :usa
           (geoipc/match-geo (ip->geo "24.123.64.1") buckets :other)))
    (is (= :other
           (geoipc/match-geo (ip->geo "2.167.109.1") buckets :other)))

    (is (= :other
           ;;  for this ip, maxmind returns nil for all location info
           (geoipc/match-geo (ip->geo "54.37.16.241") buckets :other)))))
